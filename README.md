# Strefa Kursów - Docker Kurs
Autorem obecnego kursu jest [Piotr Kośka](https://www.linkedin.com/in/piotrek-s-koska/?originalSubdomain=en)
>>>
Ten kurs jest traktowany jako kontynuacja do kursu [Kurs Docker - środowiska developerskie](https://strefakursow.pl/kursy/programowanie/kurs_docker_-_srodowiska_developerskie.html?ref=52966)
>>>
## Spis treści i lekcji omawianych w kursie:
1. **Wprowadzenie**  
   1.1 Wstęp  
   1.2 Jak korzystać z materiałów?  
   1.3 Co powinienieś umieć  

2. **Nasze środowisko i narzedzia**  
   2.1 Instalacja Docker CE na Ubuntu 20.04  
   2.2 Visual Studio  Code i dodatki do Docker CE  
   2.3 Core OS / Feroda Core OS / Flatcar OS - systemy przygotowane na konteneryzacje  
   2.4 FlatcarOS ignition  
   2.5 Własny DNS w Docker  
   2.6 Kontener z MySQL i pypmyadmin  
   2.7 Kontener z Nginx i Rancher 1.6  
   2.8 Konfiguracja rancher 1.6 i dodawanie hosta  
   2.9 Tworzymy Stack i Service w rancher 1.6 z wykorzystaniem LB  
   2.10 LB na External Service  
   2.11 Automatyczne Przekierowanie LB na HTTPS  
   2.12 Fail over naszej konfiguracji

3. **Docker Swarm, Docker registry, Docker images**  
   3.1 Docker hub  
   3.2 Wstęp do Docker Swarm  
   3.3 Konfiguracja Docker Swarm Manager  
   3.4 Konfiguracja węzłów Swarm (Worker)  
   3.5 Swarm Fail Over  
   3.6 Docker Swarm backup i przywracanie  
   3.7 Wstęp do Rejstrów Docker  
   3.8 Tworzymy własny rejestr  
   3.9 Instalacja i uruchomienie Swarmpit  
   3.10 Wykorzystujemy nasz prywatny rejestr w Swarmpit i Rancher  
   3.11 Portainer - plik docker compose  
   3.12 Portainer - Stack, Service, Task - debugowanie  
   3.13 Portainer - Pierwsze logowanie, rejestr i kontener 

4. **Tworzenie Obrazów i Dockerfile**  
   4.1 Docker images  
   4.2 Dockerfile  
   4.3 Budowanie wydajnych obrazów  
   4.4 Jednowarstwowe obrazy Docker  
   4.5 Docker in Docker  
   4.6 Konteneryzacja aplikacji node.js  
   4.7 Aktualizacja Kontenera z Watchtower

5. **Orchiestracja, Storage**  
   5.1 Locking i Unlocking klastra swarm  
   5.2 High Availability Swarm Klaster  
   5.3 Docker Service  
   5.4 Docker inspect  
   5.5 Docker compose  
   5.6 Docker stack  
   5.7 Node Label  
   5.8 GlusterFS  
   5.9 Prometheus i cadvisor  
   5.10 Grafana  
   5.11 Load Balancing Kontener  
   5.12 Zakończenie  

## To repozytorium

- [Portainer yml plik konfiguracyjny](https://gitlab.com/helppointit/strefakursowdockerkurs/-/blob/master/portainer/portainer.yml)
- [Docker in Docker - Dockerfile plik](https://gitlab.com/helppointit/strefakursowdockerkurs/-/blob/master/own_images/dind/dockerfile)
- [Flat image - plik Dokcerfile](https://gitlab.com/helppointit/strefakursowdockerkurs/-/blob/master/own_images/flat_image/dockerfile)
- [Multi-Stage Dockerfile - pliki](https://gitlab.com/helppointit/strefakursowdockerkurs/-/tree/master/own_images/go)
- [Nginx z kursu](https://gitlab.com/helppointit/strefakursowdockerkurs/-/tree/master/own_images/nginx)
- [Node.js - aplikacja przykład](https://gitlab.com/helppointit/strefakursowdockerkurs/-/tree/master/own_images/node-js)
- [Obraz do wykorzystanie z Watchtower](https://gitlab.com/helppointit/strefakursowdockerkurs/-/tree/master/own_images/watchtower)

## Poprzedni Kurs: Kurs Docker - środowiska developerskie
Kurs poziomu pierwszego odnajdziesz tu: [Kurs Docker - środowiska developerskie](https://strefakursow.pl/kursy/programowanie/kurs_docker_-_srodowiska_developerskie.html?ref=52966)  

### Spis treści i lekcji:
1. **Wprowadzenie.**  
   1.1 Wstęp.  
   1.2 Co powinieneś wiedzieć zanim zaczniesz?  
   1.3 Oprogramowanie wykorzystane podczas kursu.  
   1.4 Czym jest Docker?  

2. **Instalacja i konfiguracja środowiska.**  
   2.1 Instalacja VirtualBox  
   2.2 Instalacja Klienta SSH  
   2.3 Instalacja Dockera w systemie Windows  
   2.4 Docker w systemie Windows - docker-machine  
   2.5 Instalacja dockera w Systemie Linux Ubuntu z repozytorium  
   2.6 Instalacja dockera w Systemie Linux Ubuntu z paczki deb  
   2.7 Instalacja dockera w Systemie Linux Ubuntu ze skryptu  
   2.8 Instalacja dockera w Systemie Linux CentOS - z wykorzystaniem klienta ssh 

3. **Start / pierwsze kroki z Docker.**  
   3.1 Hello World z docker. Test naszego środowiska  
   3.2 Docker flow. Od obrazu do kontenera  
   3.3 Flaga -ti  
   3.4 Tworzenie nazwy kontenera. Flaga --name  
   3.5 Zapisywanie stanów kontenera  
   3.6 Flaga --restart  
   3.7 Kasowanie kontenera  
   3.8 Docker flow - od kontenera do obrazu  

4. **Docker obrazy**  
   4.1 Docker Hub  
   4.2 Docker Hub - zakładanie konta  
   4.3 Docker search  
   4.4 Docker pull  
   4.5 Docker inspect  
   4.6 Docker tag  
   4.7 Docker push  
   4.8 Docker rmi  

5. **Praca z Dockerem**  
   5.1 Procesy w kontenerze  
   5.2 Procesy w kontenerze jak w systemie Linux  
   5.3 Docker cykl życia  
   5.4 Docker Inspect  
   5.5 Zarzadzanie kontenerami  

6. **Kontenery w sieci**  
   6.1 Komunikacja z hosta do kontenera  
   6.2 komunikacja między kontenerami  
   6.3 Ekspozycja portów  
   6.4 Linkowanie kontenerów i flaga --net  
   6.5 Docker network ls, inspect, create, rm  
   6.6 Docker network --ip-range i statyczny ip kontenera 

7. **Wolumeny danych**  
   7.1 Co to są wolumeny danych  
   7.2 Wolumeny danych persistent i ephemeral  
   7.3 Przykład wolumena danych z httpd  
   7.4 Docker inspect - wolumeny danych  
   7.5 Docker cp - kopiowanie plików z kontenera na host lokalny i na odwrót  
   7.6 Docker volume - create, ls, rm, prune  

8. **Dockerfile**  
   8.1 Czym są Dockerfile  
   8.2 Dockerfile FROM, MAINTAINER  
   8.3 Dockerfile RUN - customizacja obrazu i warstwy  
   8.4 Dockerfile - USER  
   8.5 Dockerfile - automatyzacja  
   8.6 Dockerfile - kolejność ma znaczenie  
   8.7 Dockerfile - env, expose, cmd  

9. **Praktyczne wykorzystanie dockera (kontenera)**  
   9.1 Kontener z serwerem www z php  
   9.2 Kontener z MySQL i Wordpress  
   9.3 Kontener z serwerem SSH  

## Lista Kursów z którymi warto się zapoznać:

### Tematyka DevOps
ścieżka kariery zwawierając pakiet kursów który pomoże Ci w poznaniu zagadnien i tematu związanego z DevOps.
Ścieżka kariery [DevOps](https://strefakursow.pl/sciezki_kariery/devops_engineer.html?ref=52966)

#### **CI/CD**
Continuous integration (CI) i Continuous Delivery (CD) to zbiór zasad, wytycznych, kultura pracy i kolekcja dobrych praktyk dotyczących pracy nad projektami informatycznymi. Dzięki nim zespół developerów ma możliwość częstszego dostarczania pewnych, przetestowanych i sprawdzonych zmian w kodzie. Implementacja tych praktyk często nazywana jest CI/CD pipeline i jest uważana za jeden z najlepszych i najefektywniejszych sposobów pracy nad projektami informatycznymi i ich rozwojem.

1. Kurs wprowadzający do systemu wspomagający prace CI/CD jakim jest [Jenkins](https://videopoint.pl/kurs/jenkins-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vpuppe.htm)
2. https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_jenkins_-_nowoczesny_workflow_ci_cd.html?ref=52966
https://videopoint.pl/kurs/openshift-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vopskv.htm


#### **Automatyzacja**
Automatyzacja zadań czy konfiguracji przyśpiesza naszą pracę i czyni konfiguracje środwiska jednolitą.

1. Automatyzacja z wykorzystaniem [Ansible - Podstawy](https://videopoint.pl/kurs/ansible-kurs-video-automatyzacja-w-it-od-podstaw-piotr-koska,vansib.htm)
2. Ansible w zadaniach praktycznych - [Kurs Ansible](https://strefakursow.pl/kursy/programowanie/kurs_ansible_-_automatyzacja_zadan_w_praktyce.html?ref=52966)

#### **Cloud**
Syatemy cloud i platformy stają sie coraz popularniejsze i warto je poznać. Nauczysz sie z tych praktycznych kursów jak je wykorzystywać.  

1. Poznaj Platformę AWS [Amazon Web Services](https://videopoint.pl/kurs/amazon-web-services-aws-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vawses.htm)

2. Poznaj platforma [Microsoft Azure](https://videopoint.pl/kurs/microsoft-azure-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vmsazu.htm)

3. Kurs dotyczący [Microsoft Azure uczący podstaw tej platformy](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_microsoft_azure_od_podstaw.html?ref=52966)

4. Poznaj platformę [Google Cloud](https://videopoint.pl/kurs/google-cloud-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vgoclo.htm)


#### **Konteneryzacja**
Konteneryzacja ułatwia konfiguracje czy uruchamianie aplikacji w twoim środowisku. Przenoszenie konfiguracji ze środwiska testowego na produkcyjne nigdy nie było takie łatwe.  

1. Kurs z tematyki konteneryzacji na silniku [Docker](https://videopoint.pl/kurs/docker-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vdockv.htm)

2. Kurs [docker dla zaawansowanych](https://strefakursow.pl/kursy/programowanie/kurs_docker_dla_zaawansowanych.html?ref=52966)

3. Kubernetes jest orchiestratorem który spina ze sobą platformę konteneryzacji w jeden wspolny klaster - [Kurs podstaw kubernetes(k8s)](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_kubernetes_od_podstaw_-_zarzadzanie_i_automatyzacja_kontenerow.html?ref=52966)

4. Kurs docker z praktycznym i przykładami - [Docker dla developera](https://strefakursow.pl/kursy/programowanie/kurs_docker_-_srodowiska_developerskie.html?ref=52966)

#### **Programowanie**
Tu znajdziesz kursy programowania w języku python i go. Tu znajdziesz pełną ścieżkę [python developera](https://strefakursow.pl/sciezki_kariery/python_developer.html?ref=52966)

1. Podstawy języka [programowania w GO](https://strefakursow.pl/kursy/programowanie/fundamenty_programowania_w_jezyku_go.html?ref=52966)

2. Programowanie w [Python - zaawansowany](https://strefakursow.pl/kursy/programowanie/kurs_python_-_zaawansowany.html?ref=52966)
3. Programowanie w [Python - średniozaawansowany](https://strefakursow.pl/kursy/programowanie/kurs_python_-_sredniozaawansowany.html?ref=52966)

#### **System Kontroli Wersji**
Git - najpopularniejszy system kontroli wersji - poznaj jego podstawy i zacznij korzystać.

1. Kurs [Git dla zaawansowanych](https://strefakursow.pl/kursy/programowanie/kurs_git_dla_zaawansowanych.html?ref=52966)
2. Kurs [Git dla poczatkujących](https://strefakursow.pl/kursy/programowanie/kurs_git_dla_poczatkujacych.html?ref=52966)


### Dla Administratora Systemów / Support IT
Systemy linux i jego narzędzia są szeroko wykorzystywane w świecie biznesoym przez korporacje z dziedziny IT - oto kilka kursów i ścieżek kariery.
1. Kurs dla [Administratora Systemu Linux](https://strefakursow.pl/sciezki_kariery/administrator_linux.html?ref=52966)

2. Kurs związany z [bezpieczeństwem środwisk IT](https://strefakursow.pl/sciezki_kariery/ekspert_ds_cyberbezpieczenstwa.html?ref=52966)

3. Administracja systemem [Linux Ubintu 20.04](https://videopoint.pl/kurs/ubuntu-20-04-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vubun2.htm)

4. Wprowadzenie do tematu [Ethical Hacking](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_ethical_hacking_i_cyberbezpieczenstwo_od_podstaw.html?ref=52966)

5. Zagadnienia [Sieciowe w systemie Linux](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_podstawy_networkingu_oraz_konfiguracji_sieci_w_linux.html?ref=52966)

6. Administracja systemem [Linux](https://strefakursow.pl/kursy/it_i_oprogramowanie/administracja_serwerem_linux.html?ref=52966)

7. Administracja systemem [Linux dla Zaawansowanych](https://strefakursow.pl/kursy/it_i_oprogramowanie/zaawansowana_administracja_systemem_linux.html?ref=52966)

8. System [Linux dla początkujących](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_linux_dla_poczatkujacych.html?ref=52966)

#### **Powłoki systemowe i narzędzia**

1. [Bash Podstawy](https://videopoint.pl/kurs/bash-kurs-video-zostan-administratorem-systemow-it-piotr-koska-piotr-tenyszyn,vbashv.htm)

2. [Bash dla zaawansowanych](https://videopoint.pl/kurs/bash-techniki-zaawansowane-kurs-video-zostan-administratorem-systemow-it-piotr-koska,vbashz.htm)

3. [Poznaj Edytor Vim](https://videopoint.pl/kurs/vim-kurs-video-zostan-administratorem-systemow-it-piotr-koska-piotr-tenyszyn,vvimv.htm)

6. [Jak pracować z Nginx](https://strefakursow.pl/kursy/it_i_oprogramowanie/kurs_nginx_-_wydajne_serwery_od_podstaw.html?ref=52966)

7. [Techniki pracy z Vim](https://strefakursow.pl/kursy/programowanie/kurs_vim_-_techniki_pracy.html?ref=52966)

8. Automatyzacja i skrypty w [bash](https://strefakursow.pl/kursy/programowanie/kurs_bash_-_skrypty_i_automatyzacja.html?ref=52966)

#### **Bazy danych**
Ścieżka karier któa pozwoli zrozumieć Ci zagadnienia związane z [bazami danych](https://strefakursow.pl/sciezki_kariery/administrator_baz_danych.html)

1. Kurs obsługi i zarządznia bazą danych [PostgresSQL](https://videopoint.pl/kurs/postgresql-kurs-video-zostan-administratorem-systemow-it-piotr-tenyszyn,vpostz.htm)

2. Praktyczny [kurs obsługi bazy danych](https://strefakursow.pl/kursy/programowanie/kurs_postgresql_-_administracja_bazami_danych.html?ref=52966)
