#!/bin/bash

if [ "$DOCKER_IN_DOCKER" = "true" ] ; then
 service docker start
 echo "Docker daemon started"
fi